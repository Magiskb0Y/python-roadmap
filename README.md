## Python cho người mới bắt đầu
---
1. Python cơ bản
   * Kiểu dữ liệu (data type)
        * Có 6 loại cơ bản: `int`, `float`, `str`, `bool`, `tuple`, `list`, `dict`, `set`.
        * Chia vào 2 nhóm: **mutable** (`list`, `dict`, `set`) và **immutable** (`int`, `float`, `str`, `bool`, `tuple`).
        * Các toán tử số học, logic, nhị phân và các phương thức thường
        * Các toán tử đặc biệt `is`, `in`.
    * Cấu trúc điều khiển (control instruction)
        * Cấu trúc rẽ nhánh: `if ...` (rẽ nhánh thiếu) và `if ... elif ... else ...` (rẽ nhánh đầy đủ).
        * Cấu trúc lặp `for` và `while`.
        * Cấu trúc lặp với các **iterable**.
    * Hàm (function)
        * Định nghĩa hàm với `def`.
        * Cơ bản về generator và coroutine.
        * Hàm `lambda` và một số hàm đặc biệt `map`, `filter`.
        * Cài đặt decorator function
    * Hướng đối tượng (OOP)
        * Phân biệt `class` và `object` (hay `instance`).
        * Cài đặt `class`.
        * Một số phương thức đặc biệt.
        * Kế thừa, cài đè trong OOP và toán tử `isinstance`.
        * Xử lí ngoại lệ
    * Input và output
        * Nhập xuất từ bàn phím, màn hình.
        * Nhập xuất file.
        * Định dạng output.
    * Module và package
        * Khái niệm module và package.
        * Các module và package hoạt động.
        * Cách tạo module và package.

2. Python nâng cao
    * Tổ chức bộ nhớ
    * Áp dụng `generator` và `coroutine` trong `pipeline process` và `asynchronous`.
    * Xử lí song song: `multithread`, `multiprocess`, `asynchronous`.

3. Kiến thức cần biết
    * Quản lí các package với [pip](https://pypi.org/project/pip/) và [pipenv](https://pipenv.readthedocs.io/en/latest/), [Anaconda](https://www.anaconda.com/).
    * Cô lập môi trường phát triển với `virtualenv`.
    * Style code với [PEP8](https://www.python.org/dev/peps/pep-0008/) và sử dụng các python linter như [pylint](https://www.pylint.org/), [flake8](http://flake8.pycqa.org/en/latest/)  . 

4. Python có thể làm gì?
   * Machine learning
        * [numpy](http://www.numpy.org/): thư viện tính toán với ma trận nhiều chiều.
        * [scipy](https://www.scipy.org/): thư viện tính toán khoa học .bao gồm các module dành cho tối ưu, xử lí tín hiệu, đại số, ...
        * [pandas](https://pandas.pydata.org/): thư viện xử lí dữ liệu cho machine learning.
        * [matplotlib](https://matplotlib.org/): thư viện vẽ biểu đồ như biều đồ cột, điểm, 3D, ...
        * [scikit-learn](https://scikit-learn.org/): thư viện sử dụng cho machine learning (đơn giản + dễ sử dụng và học).
        * [tensorflow](https://www.tensorflow.org/): thư viện cho machine learning và deeplearning (phức tạp + mạnh mẽ).
    * Web
        * [django](https://www.djangoproject.com/): dễ học, dễ làm và nhanh chóng tạo ra sản phẩm.
        * [flask](http://flask.pocoo.org/): khó học, mất nhiều thời gian để tạo ra sản phẩm. Cấu trúc cơ bản đơn giản, dùng để viết microservice.
    * GUI
        * [Tkinter](https://docs.python.org/3/library/tk.html): nằm trong bộ thư viện chuẩn của python
        * [QT](https://www.qt.io/qt-for-python)
    * Game (không khuyến khích viết game bằng python)
        * [pygame](https://www.pygame.org/news)

5. Nguồn học python
    * Website
        * [Python homepage](https://www.python.org/)
        * [pythonprogramming.net](https://pythonprogramming.net/)
        * [realpython.com](https://realpython.com/)
    * Ebook
        * [Learning Python - Mark Lutz](https://www.amazon.com/Learning-Python-5th-Mark-Lutz/dp/1449355730/ref=sr_1_3?ie=UTF8&qid=1544193334&sr=8-3&keywords=Learning+Python)
        * [Learn Python 3 the Hard Way](https://www.amazon.com/Learn-Python-Hard-Way-Introduction/dp/0134692888/ref=sr_1_1_sspa?ie=UTF8&qid=1544193334&sr=8-1-spons&keywords=Learning+Python&psc=1)

6. Thực hành
   * [Code cơ bản](https://github.com/kriadmin/30-seconds-of-python-code)
   * [Luyện tập với các bài đơn giản](https://www.hackerrank.com/domains/python)
   * Viết một webapp đơn giản với flask hoặc django
   * [Project machine learning đơn giản](https://machinelearningmastery.com/machine-learning-in-python-step-by-step/)